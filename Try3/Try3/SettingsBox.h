#pragma once
#include <SFML\Graphics.hpp>
#include "Settings.h"
#include "Button.h"

class SettingsBox:
	public sf::Drawable
{
public:
	SettingsBox(Settings *settings);
	~SettingsBox();
	void Update(sf::Vector2f mPos);
	void Update();
	void setDisplay(unsigned int set);
	std::vector<Button> getArrows();
	int getDisplay();
	std::vector<sf::Text> getKeys();

private:
	sf::RectangleShape box;
	std::vector<sf::RectangleShape> lines;
	std::vector<Button> arrows;
	std::vector<sf::Text> keys_param;
	std::vector<sf::Text> win_param;
	std::vector<sf::Text> sound_param;
	sf::Texture box_texture;
	sf::Texture line_texture;
	sf::Texture arrow_texture;
	Settings * settings;
	sf::Font font;
	bool display_keys = false;
	bool display_window = true;
	bool display_sound = false;

	


private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
	

};

