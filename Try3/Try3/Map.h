#pragma once
#include "MapCell.h"
#include <vector>
#include <string>
#include <ctime>


class Map:
	public sf::Drawable
{
public:
	Map(sf::Vector2i mSize);
	~Map();
	void SetTexture(sf::Texture text, sf::IntRect sRect);
	void SetTexture(std::string path, sf::IntRect sRect);
	void GenerateLevel(unsigned int roomNum, sf::Vector2i _minSize, sf::Vector2i _maxSize);
	sf::Vector2f getPlayerSpawn();
	void setCorners(sf::IntRect tl_corner, sf::IntRect tr_corner, sf::IntRect bl_corner, sf::IntRect br_corner);


public:
	sf::Vector2i mapSize;




protected:
	sf::Texture sourceText;
	sf::IntRect sourceRect;
	std::vector<MapCell> mapCells;
	sf::Vector2f stCell;
	unsigned int roomNumb;
	unsigned int corNumb;
	std::vector<sf::IntRect> corners;

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};