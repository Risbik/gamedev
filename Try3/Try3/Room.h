#pragma once
#include <SFML\Graphics.hpp>
#include "MapCell.h"


class Room:
	public sf::Drawable
{
public: 
	static enum Direction
	{	
		LEFT,	
		TOP,	
		RIGHT,	
		BOTTOM
	};

public:
	//x - length, y - height
	sf::Vector2i size;
	sf::Vector2i stPos;
	Direction incCor;

public:
	Room(sf::Vector2i sz, sf::Vector2i st, sf::Texture* texture, sf::IntRect * textureRect);
	Room(sf::Vector2i sz, sf::Vector2i st, sf::Texture* texture, sf::IntRect * textureRect, Direction dir);
	std::vector<MapCell>* roomCells;
	~Room();

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

