#pragma once
#include "Settings.h"
#include "ContentHolder.h"
#include "Player.h"
#include "World.h"

class GameScene:
	public sf::Drawable
{
public:
	GameScene(Settings *settings, ContentHolder* _contentHolder, Player* _player);
	~GameScene();
	void Update(sf::Vector2i mPos);
	void Update(sf::Event e);
	void Update();
	SceneType sType = SceneType::_Game;


private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;


private:
	Settings *s;
	ContentHolder *contentHolder;
	Player* player;
	World* world;

};

