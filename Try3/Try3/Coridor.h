#pragma once
#include "Room.h"
#include <SFML\Graphics.hpp>

class Coridor:
	public sf::Drawable
{




public: 
	Room::Direction prevDir;
	Room::Direction corDir;
	sf::Vector2i stPos;
	sf::Vector2i endPos;
	//x - length, y - height
	sf::Vector2i size;
	std::vector<MapCell>* coridorCells;

public:
	Coridor();
	Coridor(Room thisRoom, sf::Texture * sourceText, sf::IntRect * textureRect);
	~Coridor();



private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

