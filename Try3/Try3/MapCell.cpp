#include "MapCell.h"


MapCell::MapCell(sf::Texture * sourceText, sf::Vector2i stP, sf::Vector2i size, sf::Vector2f pos)
{
	cell.setTexture(sourceText);
	cell.setTextureRect(sf::IntRect(stP, size));
	cell.setSize(cellSize);
	cell.setPosition(pos);
}

MapCell::MapCell(sf::Texture * sourceText, sf::IntRect textRect, sf::Vector2f pos)
{
	cell.setTexture(sourceText);
	cell.setTextureRect(textRect);
	cell.setPosition(pos);
	cell.setSize(size);
}

MapCell::~MapCell()
{
}

void MapCell::SetRect(sf::Vector2i stP, sf::Vector2i size)
{
	sf::IntRect ir(stP, size);
	cell.setTextureRect(ir);
}

void MapCell::SetRect(sf::IntRect rect)
{
	cell.setTextureRect(rect);
}

void MapCell::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(cell);
}
