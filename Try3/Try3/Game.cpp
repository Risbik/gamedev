#include "Game.h"


Game::Game()
{
	contentHolder = new ContentHolder();

	player = new Player();
	player->UploadTexture(&contentHolder->getTexture("cat_fighter"));
	player->animator->AddAnimation("idle", 4, 0.125f, sf::Vector2i(10, 10), sf::Vector2i(64, 64), player->getTexture());
	player->animator->PlayAnimation("idle");
	player->Update();
	settings.load();
	if (settings.winParam.fullScr != 0)
		window = new sf::RenderWindow(sf::VideoMode(settings.winParam.width, settings.winParam.height), "Soulless", sf::Style::Fullscreen);
	else
		window = new sf::RenderWindow(sf::VideoMode(settings.winParam.width, settings.winParam.height), "Soulless");
	window->setFramerateLimit(60);
	sf::Vector2f cen;
	cen.x = settings.winParam.width / 2.f;
	cen.y = settings.winParam.height / 2.f;
	view.setCenter(cen);

	view.setSize(sf::Vector2f(settings.winParam.width, settings.winParam.height));

	window->setView(view);

	mMenu = new MainMenu(&settings, contentHolder);
}


Game::~Game()
{
	delete window;
}

void Game::UpDate()
{
	PollEvents();
	if (!window->isOpen())
		return;
	TimeSinceLastUpdate = clock.restart();
	float sec = TimeSinceLastUpdate.asSeconds();


	while (sec > TimePerFrame)
	{
		//std::cout << sec << std::endl;
		PollEvents();
		sec -= TimePerFrame;
	}

	Render();

}

bool Game::run()
{
	return window->isOpen();
}



void Game::PollEvents()
{
	if (mMenu != nullptr)
		while (window->pollEvent(e))
		{
			if (e.type == sf::Event::Closed)
				window->close();


			//if is in mainScene

			if (e.type == sf::Event::KeyPressed)
			{
				if (mMenu->changeKey)
				{
					settings.keys[mMenu->changedKey].value = e.key.code;
					mMenu->changeKey = false;
					mMenu->changedKey = 0;
				}
			}
			else if (e.type == sf::Event::MouseButtonPressed)
			{
				sf::Vector2i _mPos = sf::Mouse::getPosition(*window);
				sf::Vector2f mPos = window->mapPixelToCoords(_mPos);
				std::cout << mPos.x << " " << mPos.y << std::endl;
				_mPos.x = mPos.x;
				_mPos.y = mPos.y;
				mMenu->Update(_mPos);
			}

			if (mMenu->reCreateWindow)
			{
				delete window;
				if (settings.winParam.fullScr != 0)
					window = new sf::RenderWindow(sf::VideoMode(settings.winParam.width, settings.winParam.height), "Soulless", sf::Style::Fullscreen);
				else
					window = new sf::RenderWindow(sf::VideoMode(settings.winParam.width, settings.winParam.height), "Soulless");
				window->setView(view);
				mMenu->reCreateWindow = false;
			}


			mMenu->Update();

			if (mMenu->exitFlag)
			{
				delete mMenu;
				settings.save();
				window->close();
				break;
			}

			if (mMenu->switchScene)
			{
				SceneType ns = mMenu->nextScene;
				delete mMenu;
				mMenu = nullptr;

				if (ns == SceneType::_Game)
					gScene = new GameScene(&settings, contentHolder, player);
			}
		}

	else if (gScene != nullptr)
	{
		while (window->pollEvent(e))
		{
			gScene->Update(e);
		}
		gScene->Update();

	}


}

void Game::Render()
{


	window->clear();

	if (mMenu != nullptr)
		window->draw(*mMenu);
	else if (gScene != nullptr)
		window->draw(*gScene);

	window->display();

}


