#pragma once
#include <SFML\Graphics.hpp>
#include "HitBox.h"

class Button: public sf::Drawable
{
public:
	Button(float posX, float posY, float sizeX, float sizeY);
	Button(float posX, float posY, float sizeX, float sizeY, std::string name);
	~Button();
	bool clecked;
	sf::Vector2f getPosition();
	sf::Vector2f getSize();
	void setPos(sf::Vector2f pos);
	void setText(std::string t);
	void setTexture(sf::Texture *text);
	void setTextSize(int size);
	HitBox *hBox;
	std::string name = "";


protected:
	sf::Text *text = nullptr;
	sf::Texture *texture;
	sf::RectangleShape rShape;
	sf::Font *font;


virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

