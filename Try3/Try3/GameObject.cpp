#include "GameObject.h"



GameObject::GameObject()
{
	renderingShape.setSize(sf::Vector2f(32, 18));
	animator = new Animator(&renderingShape);
}


GameObject::~GameObject()
{
}

void GameObject::UploadTexture(sf::Texture *text)
{
	this->text = text;
	renderingShape.setTexture(this->text);
}

void GameObject::setTextRect(sf::IntRect textRect)
{
	renderingShape.setTextureRect(textRect);
}

void GameObject::setTextRect(sf::Vector2i stP, sf::Vector2i size)
{
	sf::IntRect ir(stP, size);
	setTextRect(ir);
}

void GameObject::setSize(sf::Vector2f size)
{
	this->size = size;
	renderingShape.setSize(size);
}



void GameObject::Move(float x, float y)
{
	sf::Vector2f pos(x, y);
	prevPos = pos;
	renderingShape.move(pos);
}

void GameObject::Move(sf::Vector2f pos)
{
	renderingShape.move(pos);
}

sf::Vector2f GameObject::getPos()
{
	return sf::Vector2f();
}

Inventory GameObject::getInventory() const
{
	return inventory;
}

sf::Texture * GameObject::getTexture()
{
	return text;
}

void GameObject::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(renderingShape);
}
