#pragma once
#include <SFML\Graphics.hpp>
#include <Windows.h>
#include <fstream>
#include "key.h"



struct WindowParam
{
	unsigned int width;
	unsigned int height;

	unsigned int fullScr;
};

class Settings
{
public:
	Settings();
	~Settings();
	void save();
	void load();
	unsigned int getKey(std::string paramName);
	void setKey(std::string kName, unsigned int kvalue);
	std::string gamePath = "D:\\gamedev\\Try3\\Try3";
	char getKeyByNum(unsigned int num);
	unsigned int getNumByKey(char key);
	std::vector<sf::VideoMode> getVideoModes();


public:
	std::vector<key> keys;
	std::vector<sf::VideoMode> videomodes;
	unsigned int currentVMode;
	WindowParam winParam;

	
};

