#include "Player.h"



Player::Player()
{

	sf::Vector2f s(96, 128);
	renderingShape.setSize(s);



	//animator->PlayAnimation("idle");
}


Player::~Player()
{
}

void Player::Update()
{
	if (!moveAble)
		MoveSpeed = sf::Vector2f(0, 0);

	std::string animName;
	if (MoveSpeed == sf::Vector2f(0, 0))
	{
		animName = animator->GetCurAnimName();
		if (animName != "Attack")
		if (animName != "idle")
			animator->PlayAnimation("idle");
	}
	else
	{
		animName = animator->GetCurAnimName();
		if (animName != "Attack")
		Move(MoveSpeed);



		if (MoveSpeed.x < 0)
			MoveSpeed.x += stForce;
		else if (MoveSpeed.x > 0)
			MoveSpeed.x -= stForce;

		if (MoveSpeed.y < 0)
			MoveSpeed.y += stForce;
		else if (MoveSpeed.y > 0)
			MoveSpeed.y -= stForce;


		if (animName != "Attack")
		if (animName != "Move")
			animator->PlayAnimation("Move");
	}

	if (MoveSpeed.x < 0 && MoveSpeed.x + stForce > 0)
		MoveSpeed.x = 0;
	else if (MoveSpeed.x > 0 && MoveSpeed.x - stForce < 0)
		MoveSpeed.x = 0;

	if (MoveSpeed.y < 0 && MoveSpeed.y + stForce > 0)
		MoveSpeed.y = 0;
	else if (MoveSpeed.y > 0 && MoveSpeed.y - stForce < 0)
		MoveSpeed.y = 0;

	animator->Update();
}

void Player::AddForce(sf::Vector2f force)
{
	if (moveAble)
	if (std::abs(MoveSpeed.x) + std::abs(MoveSpeed.y) < MaxSpeed)
	{
		MoveSpeed.x += force.x;
		MoveSpeed.y += force.y;
	}
}

void Player::AddForce(float x, float y)
{
	sf::Vector2f force(x, y);
	AddForce(force);
}

void Player::Attack()
{
	std::string animName = animator->GetCurAnimName();
	if (animName == "Attack")
		return;

	animator->PlayAnimation("Attack");
	animator->SetNextAnimation("idle");
}
