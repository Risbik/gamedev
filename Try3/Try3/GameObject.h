#pragma once
#include <SFML\Graphics.hpp>
#include <string>
#include "Animator.h"
#include "Inventory.h"

class GameObject:
	public sf::Drawable
{
public:
	GameObject();
	~GameObject();
	void UploadTexture(sf::Texture *text);
	void setTextRect(sf::IntRect textRect);
	void setTextRect(sf::Vector2i stP, sf::Vector2i size);
	void setSize(sf::Vector2f size);
	void Move(float x, float y);
	void Move(sf::Vector2f pos);
	sf::Vector2f getPos();
	Animator* animator;
	sf::RectangleShape renderingShape;
	Inventory getInventory() const;
	sf::Texture* getTexture();

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;


private:
	sf::Vector2f pos;
	sf::Vector2f prevPos;
	sf::Vector2f size;
	Inventory inventory;
	sf::IntRect textureRect;
	sf::Vector2i stPoint;
	sf::Vector2i sizeOfText;
	sf::Texture* text;



};

