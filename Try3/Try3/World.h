#pragma once
#include "Map.h"
#include "Player.h"
#include "GameObject.h"
#include <SFML\Graphics.hpp>
#include "ContentHolder.h"

enum bioms
{
	Dark,
	Light,
	Desert,
	Forest,
	Underground
};

enum cellType
{
	Road,
	Ground,
	Wall,
	Water,
	Lake
};

class World:
	public sf::Drawable
{
public:
	World(Player *_player);
	~World();

public:
	void Update();

protected:
	void generate();

private:
	Player* player;
	ContentHolder* contentHolder;
	Map* map;
	sf::Vector2i min_biomSize = sf::Vector2i(20, 20);
	sf::Vector2i max_biomSize = sf::Vector2i(40, 40);

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

