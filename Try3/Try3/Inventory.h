#pragma once
#include <SFML\Graphics.hpp>
#include "InventoryCell.h"
#include "Title.h"

class Inventory:
	public sf::Drawable
{
public:
	Inventory();
	~Inventory();
	bool addItem(Item item);
	//void swapItems(cellNum, Item* newItem);
	bool shown = false;
	const unsigned int maxItems = 8;
	void Update(bool LMB, sf::Vector2i mPos);
	

private:
	sf::RectangleShape box;
	Title *title;
	std::vector<InventoryCell> cells;
	std::vector<Item> items;
	

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;

};

