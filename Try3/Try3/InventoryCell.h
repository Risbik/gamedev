#pragma once
#include <SFML\Graphics.hpp>
#include "HitBox.h"
#include "Item.h"

class InventoryCell:
	public sf::Drawable
{
public:
	InventoryCell(sf::Vector2f _pos);
	~InventoryCell();
	void setItem(Item *_item);
	Item* getItem();
	HitBox *hBox;
	unsigned int num = 0;
	bool occupied = false;


public:
	sf::Vector2f getSize();

private:
	sf::RectangleShape cell;
	sf::Texture text;
	sf::Vector2f pos;
	sf::Vector2f size = sf::Vector2f(40, 40);
	sf::Vector2f i_pos;
	Item *item;
	

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;

};

