#include "Scrollable_text.h"



Scrollable_text::Scrollable_text(sf::Vector2f pos, sf::Vector2f camSize, sf::Vector2f winSize)
{
	movePerTick = camSize.y / winSize.y;

	box.setSize(camSize);
	box.setPosition(pos);

	line.setSize(sf::Vector2f(line_x, camSize.y));
	line.setPosition(pos.x + camSize.x - line_x, pos.y);

	squareSize = sf::Vector2f(20.f, 20.f);

	square.setSize(squareSize);
	square.setPosition(line.getPosition());

	sf::Vector2f center(pos.x + camSize.x / 2.f, pos.y + camSize.y / 2.f);
	view.setCenter(center);
	view.setSize(camSize);
}


Scrollable_text::~Scrollable_text()
{
}

void Scrollable_text::Update(sf::Vector2f mPos)
{
	mPos.x = square.getPosition().x;


	float y = (mPos.y - square.getPosition().y) * movePerTick;



	square.setPosition(mPos);
}

void Scrollable_text::Update(unsigned int tick)
{
}

sf::View Scrollable_text::getView()
{
	return view;
}



void Scrollable_text::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	
}
