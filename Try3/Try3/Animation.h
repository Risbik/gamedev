#pragma once
#include <SFML/Graphics.hpp>
#include <string>


class Animation
{
public:
	Animation(std::string name, unsigned int frameNum, float switchTime, sf::IntRect * intrects, sf::Texture * text);
	~Animation();
	unsigned int getCurFrame();
	bool LastFrame();
	sf::IntRect NextFrame();
	std::string getName();


public:
	float switchTime;




private:
	sf::Texture texture;
	sf::IntRect* intrects;
	sf::IntRect* firstRect;
	sf::RectangleShape shape;
	unsigned int framesNum;
	unsigned int curFrame;

	std::string name;
};

