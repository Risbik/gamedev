#include "Item.h"



Item::Item()
{
}


Item::~Item()
{
}

void Item::setPos(sf::Vector2f _pos)
{
	pos = _pos;
	item.setPosition(pos);
}

void Item::setSize(sf::Vector2f _size)
{
	size = _size;
	item.setSize(size);
}

sf::Vector2f Item::getPos()
{
	return pos;
}

sf::Vector2f Item::getSize()
{
	return size;
}

sf::RectangleShape Item::getShape()
{
	return item;
}

void Item::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(item);
}
