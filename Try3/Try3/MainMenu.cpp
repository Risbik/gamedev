#include "MainMenu.h"



MainMenu::MainMenu(Settings *s, ContentHolder* _contentHolder)
{
	this->game_settings = s;

	contentHolder = _contentHolder;

	sBox = new SettingsBox(s);
	bText = contentHolder->getTexture("button");

	sf::Vector2i bSize(300, 80);
	sf::Vector2i space(10, 40);

	//main buttons
	{
		startGame = new Button(20, game_settings->winParam.height / 4, bSize.x, bSize.y);
		startGame->setText("Start game");
		startGame->setTexture(&bText);

		settings = new Button(20, startGame->getPosition().y + bSize.y + space.y, bSize.x, bSize.y);
		settings->setText("Settings");
		settings->setTexture(&bText);

		credits = new Button(20, settings->getPosition().y + bSize.y + space.y, bSize.x, bSize.y);
		credits->setText("Credits");
		credits->setTexture(&bText);

		exitButton = new Button(20, credits->getPosition().y + bSize.y + space.y, bSize.x, bSize.y);
		exitButton->setText("Exit");
		exitButton->setTexture(&bText);
	}


	//background
	{
		//bgText = new sf::Texture("");
		rShape = new sf::RectangleShape();
		//rShape->setTexture(bgText);
		rShape->setSize(sf::Vector2f(game_settings->winParam.width, game_settings->winParam.height));

		rShape->setFillColor(sf::Color(71, 74, 81));
	}

	//startGame buttons
	{
		createCharacter = new Button(20, game_settings->winParam.height - game_settings->winParam.height / 4, bSize.x, bSize.y / 1.5f);
		createCharacter->setText("Create character");
		createCharacter->setTexture(&bText);

		deleteCharacter = new Button(createCharacter->getPosition().x + bSize.x + space.x, game_settings->winParam.height - game_settings->winParam.height / 4, bSize.x, bSize.y / 1.5f);
		deleteCharacter->setText("Delete character");
		deleteCharacter->setTexture(&bText);

		choseCharacter = new Button(deleteCharacter->getPosition().x + bSize.x + space.x, game_settings->winParam.height - game_settings->winParam.height / 4, bSize.x, bSize.y / 1.5f);
		choseCharacter->setText("Select character");
		choseCharacter->setTexture(&bText);

		exitCharCreation.x = choseCharacter->getPosition().x + bSize.x + space.x;
		exitCharCreation.y = game_settings->winParam.height - game_settings->winParam.height / 4;
		 
		backToMenu = new Button(exitCharCreation.x, exitCharCreation.y, bSize.x, bSize.y / 1.5f);
		backToMenu->setText("Back to menu");
		backToMenu->setTexture(&bText);
	}




	//settings text
	{
		graphicsSet = new Button(20, game_settings->winParam.height / 4, bSize.x, bSize.y);
		graphicsSet->setText("Graphic settings");
		graphicsSet->setTexture(&bText);



		soundSet = new Button(20, graphicsSet->getPosition().y + bSize.y + space.y, bSize.x, bSize.y);
		soundSet->setText("Sound settings");
		soundSet->setTexture(&bText);

		keyboardSet = new Button(20, soundSet->getPosition().y + bSize.y + space.y, bSize.x, bSize.y);
		keyboardSet->setText("Keyboard settings");
		keyboardSet->setTexture(&bText);
	
		exitSettings.x = 20;
		exitSettings.y = keyboardSet->getPosition().y + bSize.y + space.y;

	}

}


MainMenu::~MainMenu()
{
	delete startGame;
	delete settings;
	delete credits;
	delete exitButton;

	delete bgText;
	delete rShape;

	delete backToMenu;

	delete createCharacter;
	delete choseCharacter;
	delete deleteCharacter;


}

void MainMenu::Update(sf::Vector2i mPos)
{
	sf::Vector2f size(1, 1);
	sf::Vector2f pos(mPos.x, mPos.y);

	HitBox temp(size, pos, false);

	//if main buttons is active
	if (mainButtons)
	{
		if (exitButton->hBox->checkCollision(temp))
			exitFlag = true;

		if (startGame->hBox->checkCollision(temp))
		{
			nextScene = _Game;
			switchScene = true;
			mainButtons = false;
			champSelect = true;
			backToMenu->setPos(exitCharCreation);
		}

		if (credits->hBox->checkCollision(temp))
		{
			mainButtons = false;
			creditsShow = true;
		}

		if (settings->hBox->checkCollision(temp))
		{
			mainButtons = false;
			setButtons = true;
			backToMenu->setPos(exitSettings);
		}
		return;
	}


	//if champ select buttons is active
	if (champSelect)
	{
		if (createCharacter->hBox->checkCollision(temp))
		{

		}

		if (deleteCharacter->hBox->checkCollision(temp))
		{

		}

		if (backToMenu->hBox->checkCollision(temp))
		{
			champSelect = false;
			mainButtons = true;
		}
		return;
	}


	//if settings buttons is active
	if (settings)
	{
		if (graphicsSet->hBox->checkCollision(temp))
		{
			sBox->setDisplay(0);
		}

		if (soundSet->hBox->checkCollision(temp))
		{
			sBox->setDisplay(2);
		}

		if (keyboardSet->hBox->checkCollision(temp))
		{
			sBox->setDisplay(1);
		}

		if (backToMenu->hBox->checkCollision(temp))
		{
			champSelect = false;
			mainButtons = true;
		}

		//if keys are displayed
		if (sBox->getDisplay() == 0)
		{
			std::vector<sf::Text> t = sBox->getKeys();

			HitBox *hBox;

			for (int i = 0; i < t.size(); ++i)
			{
				sf::Vector2f pos = t[i].getPosition();
				sf::Vector2f size;
				size.x = t[i].getLocalBounds().width;
				size.y = t[i].getLocalBounds().height;
				hBox = new HitBox(pos, size, false);

				if (hBox->checkCollision(temp))
				{
					changeKey = true;

					changedKey = i;
					return;
				}

				delete hBox;
			}

		}

		//if graphic settings are displayed
		if (sBox->getDisplay() == 1)
		{
			std::vector<Button> b = sBox->getArrows();
			for (unsigned int i = 0; i < b.capacity(); ++i)
			{
				if (b[i].hBox->checkCollision(temp))
				{
					Button *t = &b[i];
					if (t->name == "fs_left" || t->name == "fs_right")
						if (game_settings->winParam.fullScr > 0)
							game_settings->winParam.fullScr = 0;
						else game_settings->winParam.fullScr = 1;

					if (t->name == "sr_left")
					{
						unsigned int cW, cH;
						unsigned int sW, sH;

						cW = game_settings->winParam.width;
						cH = game_settings->winParam.height;

						for (unsigned int i = 0; i < game_settings->videomodes.size(); ++i)
						{
							if (cW == game_settings->videomodes[i].width)
								if (cH == game_settings->videomodes[i].height)
								{
									if (i == game_settings->videomodes.size() - 1)
										i = -1;

									sW = game_settings->videomodes[i + 1].width;
									sH = game_settings->videomodes[i + 1].height;

									game_settings->winParam.width = sW;
									game_settings->winParam.height = sH;
									reCreateWindow = true;
									break;
								}
						}
					}
					else if (t->name == "sr_right")
					{
						unsigned int cW, cH;
						unsigned int sW, sH;

						cW = game_settings->winParam.width;
						cH = game_settings->winParam.height;

						for (unsigned int i = 0; i < game_settings->videomodes.size(); ++i)
						{
							if (cW == game_settings->videomodes[i].width)
								if (cH == game_settings->videomodes[i].height)
								{
									if (i == 0)
										i = game_settings->videomodes.size();

									sW = game_settings->videomodes[i - 1].width;
									sH = game_settings->videomodes[i - 1].height;

									game_settings->winParam.width = sW;
									game_settings->winParam.height = sH;
									reCreateWindow = true;
									break;
								}
						}
					}

					sBox->Update();
					break;
				}
			}
		}





		return;
	}
}

void MainMenu::Update()
{
	sBox->Update();
}



void MainMenu::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(*rShape);

	if (mainButtons)
	{
		target.draw(*exitButton);
		target.draw(*startGame);
		target.draw(*credits);
		target.draw(*settings);
	}
	else if (champSelect)
	{
		target.draw(*createCharacter);
		target.draw(*choseCharacter);
		target.draw(*deleteCharacter);
		target.draw(*backToMenu);
	}
	else if (setButtons)
	{
		target.draw(*graphicsSet);
		target.draw(*soundSet);
		target.draw(*keyboardSet);
		target.draw(*backToMenu);

		target.draw(*sBox);
	}
	else if (creditsShow)
	{

	}




}
