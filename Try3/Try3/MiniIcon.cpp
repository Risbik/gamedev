#include "MiniIcon.h"



MiniIcon::MiniIcon()
{
	size.x = 16;
	size.y = 16;
	icon.setSize(size);
}

MiniIcon::MiniIcon(sf::Vector2f _pos, sf::Vector2f _size)
{
	pos = _pos;
	size = _size;

	icon.setPosition(pos);
	icon.setSize(size);
}


MiniIcon::~MiniIcon()
{
}

void MiniIcon::setPos(sf::Vector2f _pos)
{
	pos = _pos;
	icon.setPosition(pos);
}

void MiniIcon::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(icon);
}
