#pragma once
#include "GameObject.h"
class Player :
	public GameObject
{
public:
	Player();
	~Player();
	void Update();
	void AddForce(sf::Vector2f force);
	void AddForce(float x, float y);
	void Attack();
	


private:
	sf::Vector2f MoveSpeed;
	const float stForce = 0.1f;
	const float MaxSpeed = 1.f;
	bool moveAble = true;
	bool attack = false;

};

