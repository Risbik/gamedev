#pragma once
#include "Animation.h"
#include <string>

class Animator
{
public:
	Animator(sf::RectangleShape * sp);
	~Animator();
	void AddAnimation(std::string name, unsigned int frameNum, float switchTime, sf::IntRect * intrects, sf::Texture * text);
	void AddAnimation(std::string name, unsigned int frameNum, float switchTime, sf::Vector2i stP, sf::Vector2i size, sf::Texture * text);
	void PlayAnimation(std::string name);
	void SetNextAnimation(std::string name);
	void StopPlaying();
	void Update();
	std::string GetCurAnimName();

private:
	sf::RectangleShape * sprite;
	std::vector<Animation> animations;
	Animation* curAnim;
	Animation* nextAnim;
	bool playing;
	sf::Clock timer;
	sf::Time time;
	sf::RectangleShape* shape;
};

