#pragma once
#include "Button.h"
#include <SFML\Graphics.hpp>
#include "SettingsBox.h"
#include "ContentHolder.h"

class MainMenu:
	public sf::Drawable
{
public:
	MainMenu(Settings *s, ContentHolder* _contentHolder);
	~MainMenu();
	void Update(sf::Vector2i mPos);
	void Update();


public:
	Button *exitButton, *startGame, *settings, *credits;
	Button *createCharacter, *deleteCharacter, *choseCharacter;
	Button *graphicsSet, *soundSet, *keyboardSet;
	Button *backToMenu;
	sf::Texture *bgText;
	sf::Texture bText;
	sf::RectangleShape *rShape;
	bool exitFlag = false;
	bool switchScene = false;
	bool changeKey = false;
	bool reCreateWindow = false;
	unsigned int changedKey = 0;
	SceneType nextScene;

	


private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;


private:
	bool mainButtons = true;
	bool setButtons = false;
	bool champSelect = false;
	bool creditsShow = false;
	sf::Vector2f exitCharCreation;
	sf::Vector2f exitSettings;
	Settings *game_settings;
	SettingsBox *sBox;
	ContentHolder *contentHolder;


};

