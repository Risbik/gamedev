#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>
#include "GUI.h"
#include "MainMenu.h"
#include "GameScene.h"
#include "Settings.h"
#include "GameScene.h"
#include "ContentHolder.h"


class Game
{
public:
	Game();
	~Game();
	void UpDate();
	bool run();
	void exit();
	bool LMBpressed = false;
	MiniIcon *selectedIcon = nullptr;



private:
	sf::RenderWindow* window;
	sf::Event e;
	sf::Clock clock;
	sf::Time TimeSinceLastUpdate;
	const float TimePerFrame = 1.f / 60.f;
	void PollEvents();
	void Render();
	MainMenu *mMenu = nullptr;
	GameScene *gScene = nullptr;
	Settings settings;
	bool Keys[4];
	sf::View view;
	ContentHolder *contentHolder;
	Player *player;

};

