#include "Inventory.h"



Inventory::Inventory()
{

	sf::Vector2f pos(20, 20);
	sf::Vector2f size(400, 300);
	box.setPosition(pos);
	box.setSize(size);
	box.setFillColor(sf::Color::Blue);

	title = new Title(box, "Inventory");
	
	sf::Vector2f s_pos;
	unsigned int space = 40;
	s_pos.x = pos.x + space;
	s_pos.y = pos.y + space + title->getSize().y;
	for (int i = 0; i < maxItems; ++i)
	{
		InventoryCell iCell(s_pos);
		iCell.num = i;
		cells.push_back(iCell);
		s_pos.x += iCell.getSize().x + space;

		if (i == 3)
		{
			s_pos.x = pos.x + space;
			s_pos.y = pos.y + space + space + iCell.getSize().y + title->getSize().y;
		}
	}
}


Inventory::~Inventory()
{
}

bool Inventory::addItem(Item item)
{
	if (items.size() == maxItems)
		return false;
	else
		items.push_back(item);

	for (int i = 0; i < maxItems; ++i)
	{
		if (!cells[i].occupied)
		{
			cells[i].setItem(&items[items.size() - 1]);
			break;
		}
	}
}



void Inventory::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	if (shown)
	{
		target.draw(box);
		target.draw(*title);

		for (int i = 0; i < maxItems; ++i)
		{
			target.draw(cells[i]);
		}
	}
}
