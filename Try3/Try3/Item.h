#pragma once
#include <SFML\Graphics.hpp>
#include "HitBox.h"
#include "MiniIcon.h"

class Item:
	public sf::Drawable
{
public:
	Item();
	~Item();
	HitBox *hBox;
	MiniIcon miniIcon;
	void setPos(sf::Vector2f _pos);
	void setSize(sf::Vector2f _size);
	sf::Vector2f getPos();
	sf::Vector2f getSize();
	sf::RectangleShape getShape();


private:
	sf::RectangleShape item;
	sf::Texture text;
	sf::Vector2f pos;
	sf::Vector2f size;

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

