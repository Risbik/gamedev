#pragma once
#include <SFML\Graphics.hpp>

class MiniIcon:
	public sf::Drawable
{
public:
	MiniIcon();
	MiniIcon(sf::Vector2f _pos, sf::Vector2f _size);
	~MiniIcon();
	void setPos(sf::Vector2f _pos);
	unsigned int place = 0;


private:
	sf::RectangleShape icon;
	sf::Texture text;
	sf::Vector2f pos, size;



private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

