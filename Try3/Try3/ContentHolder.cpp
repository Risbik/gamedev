#include "ContentHolder.h"



ContentHolder::ContentHolder():
	textures(), fonts()
{

	sf::Texture t;
	std::string name = "button";
	t.loadFromFile("Content//Buttons//button.png");
	textures.insert(std::pair<std::string, sf::Texture>(name, t));

	name = "cat_fighter";
	t.loadFromFile("Content//Sprites//cat_fighter.png");
	textures.insert(std::pair<std::string, sf::Texture>(name, t));

}


ContentHolder::~ContentHolder()
{
}

sf::Texture & ContentHolder::getTexture(std::string path)
{
	auto pairFound = textures.find(path);
	if (pairFound != textures.end()) {

		return pairFound->second;
	}
	else
	{
		auto texture = textures[path];
		texture.loadFromFile(path);
		return texture;
	}
}

sf::Font & ContentHolder::getFont(std::string path)
{
	auto pairFound = fonts.find(path);
	if (pairFound != fonts.end()) {

		return pairFound->second;
	}
	else
	{
		auto font = fonts[path];
		font.loadFromFile(path);
		return font;
	}
}

/*
sf::SoundBuffer ContentHolder::getSound(std::string path)
{
	auto pairFound = sounds.find(path);
	if (pairFound != sounds.end())
	{
		return pairFound->second;
	}
	else
	{

		auto& sBuffer = sounds[path];
		sBuffer.loadFromFile(path);
		return sBuffer;
	}
}*/
