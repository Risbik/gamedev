#include "World.h"



World::World()
{
}


World::World(Player * _player)
{
	player = _player;
	generate();
}

World::~World()
{
}

void World::generate()
{
	map = new Map(sf::Vector2i(30, 30));
	map->GenerateLevel(5);
}

void World::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(*map);
	target.draw(*player);
}
