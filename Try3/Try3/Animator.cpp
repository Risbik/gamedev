#include "Animator.h"



Animator::Animator(sf::RectangleShape * sp)
{
	sprite = sp;
	nextAnim = nullptr;
}


Animator::~Animator()
{
}

void Animator::AddAnimation(std::string name, unsigned int frameNum, float switchTime, sf::IntRect * intrects, sf::Texture * text)
{
	Animation tmp(name, frameNum, switchTime, intrects, text);

	animations.push_back(tmp);
}

void Animator::AddAnimation(std::string name, unsigned int frameNum, float switchTime, sf::Vector2i stP, sf::Vector2i size, sf::Texture * text)
{
	sf::IntRect * ir = new sf::IntRect[frameNum];
	for (int i = 0; i < frameNum; i++)
	{
		ir[i].left = stP.x + size.x*i;
		ir[i].top = stP.y;
		ir[i].width = size.x;
		ir[i].height = size.y;
	}

	AddAnimation(name, frameNum, switchTime, ir, text);
}

void Animator::PlayAnimation(std::string name)
{
	for (int i = 0; i < animations.capacity(); i++)
	{
		std::string nm = animations[i].getName();
		if (nm == name)
		{
			curAnim = &animations[i];
			break;
		}
	}

	playing = true;
	timer.restart();
}

void Animator::SetNextAnimation(std::string name)
{
	for (int i = 0; i < animations.capacity(); i++)
	{
		std::string nm = animations[i].getName();
		if (nm == name)
		{
			nextAnim = &animations[i];
			break;
		}
	}
}

void Animator::StopPlaying()
{
	playing = false;
}

void Animator::Update()
{
	if (playing)
	{
		time = timer.getElapsedTime();
		float sec = time.asSeconds();

		if (sec >= curAnim->switchTime)
		{
			sprite->setTextureRect(curAnim->NextFrame());
			timer.restart();
		}
	}
	if (nextAnim != nullptr)
		if (curAnim->LastFrame())
		{
			curAnim = nextAnim;
			nextAnim = nullptr;
		}
}

std::string Animator::GetCurAnimName()
{
	return curAnim->getName();
}
