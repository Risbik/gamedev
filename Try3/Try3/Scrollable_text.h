#pragma once
#include <SFML\Graphics.hpp>
#include "Button.h"



class Scrollable_text:
	public sf::Drawable
{
public:
	Scrollable_text(sf::Vector2f pos, sf::Vector2f camSize, sf::Vector2f winSize);
	~Scrollable_text();
	void Update(sf::Vector2f mPos);
	void Update(unsigned int tick);
	sf::View getView();


public:
	std::vector<Button> buttons;
	std::vector<sf::Text> text;


private:
	sf::RectangleShape square;
	sf::RectangleShape line;
	sf::RectangleShape box;
	sf::Font font;
	sf::Vector2f textSize;
	float movePerTick = 0.f;
	float line_x = 20.f;
	sf::Vector2f squareSize;
	sf::View view;


private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

