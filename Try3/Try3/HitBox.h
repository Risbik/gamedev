#pragma once
#include <SFML\Graphics.hpp>

class HitBox:
	public sf::Drawable
{
public:
	HitBox(sf::Vector2f pos, sf::Vector2f size, bool vis);
	~HitBox();
	bool checkCollision(HitBox hBox);
	void setVisible(bool set);
	void setPos(float x, float y);
	void setSize(float x, float y);
	sf::Vector2f getPos();
	sf::Vector2f getSize();



private:
	sf::RectangleShape rShape;
	bool visible = false;

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

