#include "InventoryCell.h"



InventoryCell::InventoryCell(sf::Vector2f _pos)
{
	pos = _pos;
	cell.setPosition(pos);
	cell.setSize(size);
	cell.setOutlineThickness(10.f);
	cell.setOutlineColor(sf::Color::Green);
	hBox = new HitBox(pos, size, false);

	i_pos.x = pos.x + 2;
	i_pos.y = pos.y + 2;
	//icon.setPos(i_pos);
	
}


InventoryCell::~InventoryCell()
{
}

void InventoryCell::setItem(Item * _item)
{
	item = _item;
	item->miniIcon.setPos(i_pos);
	occupied = true;
}

Item * InventoryCell::getItem()
{
	return item;
}





sf::Vector2f InventoryCell::getSize()
{
	return size;
}

void InventoryCell::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(cell);
}
