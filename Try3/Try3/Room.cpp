#include "Room.h"






Room::Room(sf::Vector2i sz, sf::Vector2i st, sf::Texture* texture, sf::IntRect * textureRect)
{
	size = sz;
	stPos.x = st.x;
	stPos.y = st.y;

	roomCells = new std::vector<MapCell>[size.y];


	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
		{
			sf::Vector2i tSt;
			tSt.x = textureRect->left + textureRect->width / ((rand() % 16) + 1);
			tSt.y = textureRect->top + textureRect->height / (rand() % 16 + 1);
			sf::Vector2i sizeT;
			sizeT.x = 16;
			sizeT.y = 16;

			sf::Vector2f pos;
			pos.x = stPos.x + 16*j;
			pos.y = stPos.y + 16*i;
			MapCell mpCell(texture, tSt, sizeT, pos);


			roomCells[i].push_back(mpCell);
		}


	//��������� ������ �������
	{
		sf::Vector2i cellSize;
		sf::Vector2i stPos;
		cellSize.x = 16; cellSize.y = 16;

		//������ � ������� �������
		for (int i = 0; i < size.x; i++)
		{
			stPos.x = textureRect->left + textureRect->width / ((rand() % 16) + 1);
			stPos.y = textureRect->top;
			roomCells[0][i].SetRect(stPos, cellSize);

			stPos.x = textureRect->left + textureRect->width / ((rand() % 16) + 1);
			stPos.y = textureRect->top + textureRect->height - 16;
			roomCells[(size.y - 1)][i].SetRect(stPos, cellSize);
		}

		//������ � ����� �������
		for (int i = 0; i < size.y; i++)
		{
			stPos.x = textureRect->left;
			stPos.y = textureRect->top + textureRect->height / ((rand()) % 16 + 1);
			roomCells[i][0].SetRect(stPos, cellSize);

			stPos.x = textureRect->left + textureRect->width - 16;
			stPos.y = textureRect->top + textureRect->height / ((rand()) % 16 + 1);
			roomCells[i][size.x - 1].SetRect(stPos, cellSize);
		}


		//���� ����
		stPos.x = textureRect->left;
		stPos.y = textureRect->top;
		roomCells[0][0].SetRect(stPos, cellSize);

		//���� �����
		stPos.x = textureRect->left + textureRect->width - 16;
		stPos.y = textureRect->top;
		roomCells[0][size.x - 1].SetRect(stPos, cellSize);

		//��� ����
		stPos.x = textureRect->left;
		stPos.y = textureRect->top + textureRect->height - 16;
		roomCells[size.y - 1][0].SetRect(stPos, cellSize);

		//��� �����
		stPos.x = textureRect->left + textureRect->width - 16;
		stPos.y = textureRect->top + textureRect->height - 16;
		roomCells[size.y - 1][size.x - 1].SetRect(stPos, cellSize);

	}
}

Room::Room(sf::Vector2i sz, sf::Vector2i st, sf::Texture* texture, sf::IntRect * textureRect, Direction dir)
	:Room(sz,st,texture,textureRect)
{
	incCor = dir;
}

Room::~Room()
{
}

void Room::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
			target.draw(roomCells[i][j]);
}
