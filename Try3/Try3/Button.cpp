#include "Button.h"



Button::Button(float posX, float posY, float sizeX, float sizeY)
{
	sf::Vector2f pos(posX, posY);
	sf::Vector2f size(sizeX, sizeY);
	rShape.setPosition(pos);
	rShape.setSize(size);
	rShape.setTexture(texture);
	//rShape.setFillColor(sf::Color(255,127,127,127));



	hBox = new HitBox(pos, size, false);
}

Button::Button(float posX, float posY, float sizeX, float sizeY, std::string name)
	:Button(posX, posY, sizeX, sizeY)
{
	this->name = name;
}

Button::~Button()
{
	delete text;
	delete font;

	//delete hBox;
}

sf::Vector2f Button::getPosition()
{
	return rShape.getPosition();
}

sf::Vector2f Button::getSize()
{
	return rShape.getSize();
}

void Button::setPos(sf::Vector2f pos)
{
	rShape.setPosition(pos);
	hBox->setPos(pos.x, pos.y);

	if (text == nullptr)
		return;
	float x = rShape.getPosition().x + rShape.getSize().x / 2.f - text->getLocalBounds().width / 2.f - text->getLocalBounds().left;
	float y = rShape.getPosition().y + rShape.getSize().y / 2.f - text->getLocalBounds().height / 2.f - text->getLocalBounds().top;
	text->setPosition(x, y);
}

void Button::setText(std::string t)
{
	font = new sf::Font();
	font->loadFromFile("Content//Hunters.otf");
	text = new sf::Text();
	text->setString(t);
	text->setCharacterSize(30);
	text->setFont(*font);
	int length = t.length();

	text->setFillColor(sf::Color::Green);


	float x = rShape.getPosition().x + rShape.getSize().x / 2.f - text->getLocalBounds().width / 2.f - text->getLocalBounds().left;
	float y = rShape.getPosition().y + rShape.getSize().y / 2.f - text->getLocalBounds().height / 2.f - text->getLocalBounds().top;
	text->setPosition(x,y);
}

void Button::setTexture(sf::Texture *text)
{
	texture = text;
	rShape.setTexture(texture);
	//rShape.setScale(1.5f, 2.f);
}

void Button::setTextSize(int size)
{
	text->setCharacterSize(size);
}

void Button::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(rShape);
	if (text != nullptr)
	target.draw(*text);
}
