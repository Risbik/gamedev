#include "SettingsBox.h"



SettingsBox::SettingsBox(Settings *settings)
{
	this->settings = settings;


	font.loadFromFile("Content//Hunters.otf");
	
	
	sf::Vector2f box_pos;
	sf::Vector2f box_size;

	box_size.x = settings->winParam.width / 4.f;
	box_size.y = settings->winParam.height / 1.5f;

	box_pos.x = settings->winParam.width - settings->winParam.width / 2.f - box_size.x / 2.f;
	box_pos.y = settings->winParam.width * .1f;

	box.setPosition(box_pos);
	box.setSize(box_size);
	box.setFillColor(sf::Color::Magenta);
	

	sf::RectangleShape line;
	sf::Vector2f line_size;
	sf::Vector2f line_startPos;
	sf::Vector2f line_pos;

	line.setFillColor(sf::Color::Black);
	line_size.x = box_size.x * 0.8f;
	line_size.y = 5.f;

	line_startPos.x = box_pos.x + line_size.x * .1f;
	line_startPos.y = box_pos.y + box_size.y * .2f;

	sf::Text temp;
	temp.setFont(font);
	temp.setCharacterSize(30);
	temp.setFillColor(sf::Color::Blue);

	line.setSize(line_size);
	line.setPosition(line_startPos);

	line_pos = line_startPos;
	
	//keys output
	for (unsigned int i = 0; i < settings->keys.size(); ++i)
	{

		line.setSize(line_size);
		line.setPosition(line_pos);
		lines.push_back(line);

		std::string pName = settings->keys[i].paramName;
		unsigned int pValue = settings->keys[i].value;

		temp.setString(pName);



		sf::Vector2f text_pos;
		text_pos.x = line.getPosition().x;
		text_pos.y = line.getPosition().y - temp.getLocalBounds().height / 2.f - temp.getLocalBounds().top - 5;

		temp.setPosition(text_pos);
		keys_param.push_back(temp);



		temp.setString(settings->getKeyByNum(pValue));

		text_pos.x = line.getPosition().x + line.getSize().x - temp.getLocalBounds().left - temp.getLocalBounds().width;
		text_pos.y = line.getPosition().y - temp.getLocalBounds().height - temp.getLocalBounds().top - 5;

		temp.setPosition(text_pos);
		keys_param.push_back(temp);

		line_pos.y += line_size.y + temp.getLocalBounds().top + temp.getLocalBounds().height + 5;
	}
	
	//winparam output
	{
		line_pos.x = box_pos.x + line_size.x * .1f;
		line_pos.y = box_pos.y + box_size.y * .2f;

		std::string pName = "Screen resolution";
		std::string pValue = std::to_string(settings->winParam.width) + ":" + std::to_string(settings->winParam.height);

		temp.setString(pName);

		sf::Vector2f text_pos;
		text_pos.x = line_pos.x;
		text_pos.y = line_pos.y - temp.getLocalBounds().height / 2.f - temp.getLocalBounds().top - 5;

		sf::Vector2f b_size;
		b_size.x = 10;
		b_size.y = 10;
			   
		temp.setPosition(text_pos);

		win_param.push_back(temp);

		temp.setString(pValue);

		text_pos.x = line_pos.x + line.getSize().x - temp.getLocalBounds().left - temp.getLocalBounds().width;
		text_pos.y = line_pos.y - temp.getLocalBounds().height - temp.getLocalBounds().top - 5;
		
		temp.setPosition(text_pos);

		win_param.push_back(temp);

		sf::Vector2f b_pos;
		b_pos.x = text_pos.x - b_size.x - 5;
		b_pos.y = line_pos.y - b_size.y * 1.5f;

		Button *t = new Button(b_pos.x, b_pos.y, b_size.x, b_size.y, "sr_left");
		arrows.push_back(*t);
		delete t;
		b_pos.x = temp.getPosition().x + temp.getLocalBounds().left + temp.getLocalBounds().width + b_size.x;


		t = new Button(b_pos.x, b_pos.y, b_size.x, b_size.y, "sr_right");
		arrows.push_back(*t);
		delete t;


		line_pos.y += line_size.y + temp.getLocalBounds().top + temp.getLocalBounds().height + 5;

		pName = "Fullscreen";
		if (settings->winParam.fullScr != 0)
			pValue = "yes";
		else
			pValue = "no";

		temp.setString(pName);

		text_pos.x = line_pos.x;
		text_pos.y = line_pos.y - temp.getLocalBounds().height / 2.f - temp.getLocalBounds().top - 5;

		temp.setPosition(text_pos);

		win_param.push_back(temp);

		temp.setString(pValue);

		text_pos.x = line_pos.x + line.getSize().x - temp.getLocalBounds().left - temp.getLocalBounds().width;
		text_pos.y = line_pos.y - temp.getLocalBounds().height - temp.getLocalBounds().top - 5;
		
		temp.setPosition(text_pos);
		win_param.push_back(temp);

		b_pos.x = text_pos.x - b_size.x - 5;
		b_pos.y = line_pos.y - b_size.y * 1.5f;

		t = new Button(b_pos.x, b_pos.y, b_size.x, b_size.y, "fs_left");
		arrows.push_back(*t);
		delete t;

		b_pos.x = temp.getPosition().x + temp.getLocalBounds().left + temp.getLocalBounds().width + b_size.x;

		t = new Button(b_pos.x, b_pos.y, b_size.x, b_size.y, "fs_right");
		arrows.push_back(*t);
		delete t;
	}
}


SettingsBox::~SettingsBox()
{
}

void SettingsBox::Update(sf::Vector2f mPos)
{
}

void SettingsBox::Update()
{
	unsigned int t = 0;
	for (unsigned int i = 0; i < keys_param.size(); i += 2)
	{
		if (i == keys_param.size())
			break;

		keys_param[i].setString(settings->keys[t].paramName);
		char temp = settings->getKeyByNum(settings->keys[t].value);
		keys_param[i+1].setString(temp);
		t++;
	}


		std::string temp = "Screen resolution";
		win_param[0].setString(temp);
		temp = std::to_string(settings->winParam.width) + ":" + std::to_string(settings->winParam.height);
		win_param[1].setString(temp);
		temp = "Fullscreen";
		win_param[2].setString(temp);
		if (settings->winParam.fullScr > 0)
			temp = "yes";
		else temp = "no";
		win_param[3].setString(temp);	
}

void SettingsBox::setDisplay(unsigned int set)
{
	display_window = false;
	display_keys = false;
	display_sound = false;

	if (set == 0)
		display_window = true;
	else if (set == 1)
		display_keys = true;
	else if (set == 2)
		display_sound = true;
}

std::vector<Button> SettingsBox::getArrows()
{
	return arrows;
}

int SettingsBox::getDisplay()
{
	if (display_keys)
		return 0;
	if (display_window)
		return 1;
	if (display_sound)
		return 2;
}

std::vector<sf::Text> SettingsBox::getKeys()
{
	std::vector<sf::Text> t;

	for (unsigned int i = 1; i < keys_param.size(); i += 2)
	{
		t.push_back(keys_param[i]);
	}
	return t;
}



void SettingsBox::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(box);
	if (display_keys)
	{
		for (unsigned int i = 0; i < lines.size(); ++i)
			target.draw(lines[i]);

		for (unsigned int i = 0; i < keys_param.size() - 1; ++i)
			target.draw(keys_param[i]);
	}

	if (display_window)
	{
		for (unsigned int i = 0; i < win_param.size(); ++i)
			target.draw(lines[i]);

		for (unsigned int i = 0; i < win_param.size(); ++i)
			target.draw(win_param[i]);
		for (unsigned int i = 0; i < arrows.size(); ++i)
			target.draw(arrows[i]);
	}

	if (display_sound)
	{

	}
}
