#include "Animation.h"



Animation::Animation(std::string name, unsigned int frameNum, float switchTime, sf::IntRect * intrects, sf::Texture * text)
{
	this->name = name;
	this->framesNum = frameNum;
	this->switchTime = switchTime;
	this->intrects = new sf::IntRect[frameNum];
	firstRect = this->intrects;

	int tmp = 0;
	while (true)
	{
		this->intrects[tmp] = *intrects;
		tmp++;
		if (tmp >= framesNum)
			break;
		intrects++;
	}

	curFrame = 0;

	shape.setTexture(text);
	shape.setTextureRect(this->intrects[curFrame]);
}


Animation::~Animation()
{
}

unsigned int Animation::getCurFrame()
{
	return curFrame;
}

bool Animation::LastFrame()
{
	if (curFrame == framesNum - 1)
	{
		curFrame = 0;
		return true;
	}
	else
		return false;
}



sf::IntRect Animation::NextFrame()
{
	curFrame++;
	if (curFrame >= framesNum)
	{
		curFrame = 0;
		return intrects[curFrame];
	}
	else
	{
		return intrects[curFrame];
	}
}

std::string Animation::getName()
{
	return name;
}

