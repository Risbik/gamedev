#include "Title.h"



Title::Title(sf::RectangleShape r_shape, std::string _text)
{
	sf::Vector2f pos = r_shape.getPosition();
	sf::Vector2f size;
	size.x = r_shape.getSize().x;
	size.y = 40;

	box.setPosition(pos);
	box.setSize(size);
	box.setFillColor(sf::Color::Magenta);

	text.setString(_text);
	font.loadFromFile("Content//Hunters.otf");
	text.setFont(font);

	sf::Vector2f t_pos;

	t_pos.x = pos.x + size.x / 2.f - text.getGlobalBounds().left - text.getGlobalBounds().width / 2.f;
	t_pos.y = pos.y - text.getGlobalBounds().height / 2;

	text.setPosition(t_pos);


}


Title::~Title()
{
}

sf::Vector2f Title::getSize()
{
	return box.getSize();
}



void Title::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(box);
	target.draw(text);
}
