#pragma once
#include <SFML\Graphics.hpp>


class MapCell:
	public sf::Drawable
{
public:
	MapCell(sf::Texture* sourceText, sf::Vector2i stP, sf::Vector2i size, sf::Vector2f pos);
	MapCell(sf::Texture* sourceText, sf::IntRect textRect, sf::Vector2f pos);
	~MapCell();
	void SetRect(sf::Vector2i stP, sf::Vector2i size);
	void SetRect(sf::IntRect rect);

public:
	const sf::Vector2f cellSize = sf::Vector2f(20.f, 20.f);
	unsigned int cellNumX;
	unsigned int cellNumY;



private:
	sf::RectangleShape cell;
	sf::Vector2f size = sf::Vector2f(16, 16);




private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

