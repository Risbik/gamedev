#pragma once
#include <SFML\Graphics.hpp>
class Title:
	public sf::Drawable
{
public:
	Title(sf::RectangleShape r_shape, std::string _text);
	~Title();
	sf::Vector2f getSize();

private:
	sf::Text text;
	sf::Font font;
	sf::RectangleShape box;

private:
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const;
};

