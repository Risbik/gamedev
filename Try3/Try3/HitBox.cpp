#include "HitBox.h"





bool HitBox::checkCollision(HitBox hBox)
{
	sf::Vector2f hBoxPos = hBox.getPos();
	sf::Vector2f hBoxSize = hBox.getSize();

	sf::Vector2f pos = rShape.getPosition();
	sf::Vector2f size = rShape.getSize();

	if (pos.y < hBoxPos.y)
	{
		if (pos.y + size.y >= hBoxPos.y)
		{
			if (pos.x < hBoxPos.x)
			{
				if (pos.x + size.x >= hBoxPos.x)
				{
					return true;
				};
			}
			else if (pos.x <= hBoxPos.x + hBoxSize.x)
			{
				return true;
			};
		};
	}
	else if (pos.y <= hBoxPos.y + hBoxSize.y)
	{
		if (pos.x < hBoxPos.x)
		{
			if (pos.x + size.x >= hBoxPos.x)
			{
				return true;
			};
		}
		else if (pos.x <= hBoxPos.x + hBoxSize.x)
		{
			return true;
		};

	};
	return false;
}

HitBox::HitBox(sf::Vector2f pos, sf::Vector2f size, bool vis)
{
	rShape.setPosition(pos);
	rShape.setSize(size);
	visible = vis;
}


HitBox::~HitBox()
{

}

void HitBox::setVisible(bool set)
{
	visible = set;
}

void HitBox::setPos(float x, float y)
{
	sf::Vector2f pos(x, y);
	rShape.setPosition(pos);
}

void HitBox::setSize(float x, float y)
{
	sf::Vector2f size(x, y);
	rShape.setSize(size);
}

sf::Vector2f HitBox::getPos()
{
	return rShape.getSize();
}

sf::Vector2f HitBox::getSize()
{
	return rShape.getPosition();
}

void HitBox::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	if (visible)
		target.draw(rShape);
}
