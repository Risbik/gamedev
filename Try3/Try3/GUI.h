#pragma once
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "Inventory.h"


class GUI
{
public:
	GUI();
	~GUI();

private:
	Inventory inventory;
	bool showInv = false;
	Button *closeInv;
};

