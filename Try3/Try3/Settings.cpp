#include "Settings.h"



Settings::Settings()
{
	videomodes = sf::VideoMode::getFullscreenModes();
}


Settings::~Settings()
{
}

void Settings::save()
{
	std::string path = gamePath + "\\Saves\\settings.ini";

	std::ofstream file;
	file.open(path);

	sf::VideoMode vMode = sf::VideoMode::getDesktopMode();

	file << "[" << "Window params" << "]" << std::endl;
	{
		file << "height" << "=" << this->winParam.height << std::endl;
		file << "width" << "=" << this->winParam.width << std::endl;
		file << "fullscreen" << "=" << this->winParam.fullScr << std::endl;
	}









	file << "[" << "Keys" << "]" << std::endl;
	for (unsigned int i = 0; i < keys.capacity(); ++i)
	{
		file << keys[i].paramName << "=" << keys[i].value << std::endl;
	}
	   
	file.close();
}

void Settings::load()
{
	std::string path = gamePath + "\\Saves\\settings.ini";


	std::ifstream file;
	file.open(path);
	std::string data;



	key temp_key;

	std::getline(file, data);
	if (data == "[Window params]")
	{
		for (unsigned int j = 0; j < 3; ++j)
		{
			std::getline(file, data);

			int i = 0;

			std::string temp = "";

			while (data[i] != '=')
			{
				temp += data[i];
				i++;
			}

			i++;
			temp_key.paramName = temp;

			temp = "";
			
			while (i < data.length())
			{
				temp += data[i];
				i++;
			}

			temp_key.value = std::atoi(temp.c_str());

			if (j == 0)
				winParam.height = temp_key.value;
			else if (j == 1)
				winParam.width = temp_key.value;
			else
				winParam.fullScr = temp_key.value;


		}
	}

	std::getline(file, data);
	if (data == "[Keys]")
	{
		std::getline(file, data);
		while(data != "")
		{
			if ((temp_key.paramName + "=" + std::to_string(temp_key.value)) == data)
				break;

			int i = 0;

			std::string temp = "";

			while (data[i] != '=')
			{
				temp += data[i];
				i++;
			}
			i++;

			temp_key.paramName = temp;

			temp = "";

			while (i < data.length())
			{
				temp += data[i];
				i++;
			}

			temp_key.value = std::atoi(temp.c_str());

			keys.push_back(temp_key);
			std::getline(file, data);
		}
	}
	

	file.close();
}

unsigned int Settings::getKey(std::string paramName)
{
	for (int i = 0; i < keys.capacity(); ++i)
		if (keys[i].paramName == paramName)
			return keys[i].value;
}

void Settings::setKey(std::string kName, unsigned int kvalue)
{
	for (int i = 0; i < keys.capacity(); ++i)
	{
		if (keys[i].paramName == kName)
		{
			keys[i].value = kvalue;
			return;
		}
	}
}

char Settings::getKeyByNum(unsigned int num)
{
	return (char)(65 + num);
}

unsigned int Settings::getNumByKey(char key)
{
	return ((int)key - 65);
}

std::vector<sf::VideoMode> Settings::getVideoModes()
{
	return videomodes;
}
