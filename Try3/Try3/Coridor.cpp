#include "Coridor.h"



Coridor::Coridor()
{
}

Coridor::Coridor(Room thisRoom, sf::Texture * sourceText, sf::IntRect * textureRect)
{
	prevDir = thisRoom.incCor;


	



	srand(std::time(NULL));
	corDir = Room::Direction(rand() % 4);

	if ((int)prevDir == (int)corDir)
		corDir = (Room::Direction) (corDir + 1 % 4);

	unsigned int kX, kY;
	srand(std::time(NULL));
	if (corDir == Room::BOTTOM)
	{
		kY = 1;
		kX = 1;
		stPos.x = (thisRoom.stPos.x + thisRoom.size.x * 20) / ((rand() % 5) + 1);
		stPos.y = thisRoom.stPos.y + thisRoom.size.y * 20;


		size.y = rand() % 5 + 10;
		size.x = rand() % 3 + 3;
	}
	else if (corDir == Room::TOP)
	{
		kY = -1;
		kX = 1;
		stPos.x = (thisRoom.stPos.x + thisRoom.size.x * 20) / ((rand() % 5) + 1);
		stPos.y = thisRoom.stPos.y * 20;

		size.y = rand() % 5 + 10;
		size.x = rand() % 3 + 3;
	}
	else if (corDir == Room::LEFT)
	{
		kY = 1;
		kX = -1;
		stPos.x = thisRoom.stPos.x * 20;
		stPos.y = (thisRoom.stPos.y + thisRoom.size.y * 20) / ((rand() % 5) + 1);

		size.x = rand() % 5 + 10;
		size.y = rand() % 3 + 3;
	}
	else
	{
		kY = 1;
		kX = 1;
		stPos.x = thisRoom.stPos.x + thisRoom.size.x * 20;
		stPos.y = (thisRoom.stPos.y + thisRoom.size.y * 20) / ((rand() % 5) + 1);

		size.x = (rand() % 5 + 10);
		size.y = (rand() % 3 + 3);
	}

	endPos.x = stPos.x + size.x*kX * 20;
	endPos.y = stPos.y + size.y*kY * 20;





	coridorCells = new std::vector<MapCell>[size.y];


	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
		{
			sf::Vector2i tSt;
			tSt.x = textureRect->left + textureRect->width / ((rand() % 16) + 1);
			tSt.y = textureRect->top + textureRect->height / (rand() % 16 + 1);
			sf::Vector2i sizeT;
			sizeT.x = 16;
			sizeT.y = 16;

			sf::Vector2f pos;
			pos.x = stPos.x + 16 * j * kX;
			pos.y = stPos.y + 16 * i * kY;
			MapCell mpCell(sourceText, tSt, sizeT, pos);


			coridorCells[i].push_back(mpCell);
		}






	//��������� ������ ������
	{
		sf::Vector2i cellSize;
		sf::Vector2i stPos;
		cellSize.x = 16; cellSize.y = 16;

		//������ � ������� �������
		if (kX != 0)
		for (int i = 0; i < size.x; i++)
		{
			stPos.x = textureRect->left + textureRect->width / ((rand() % 16) + 1);
			stPos.y = textureRect->top;
			coridorCells[0][i].SetRect(stPos, cellSize);

			stPos.x = textureRect->left + textureRect->width / ((rand() % 16) + 1);
			stPos.y = textureRect->top + textureRect->height - 16;
			coridorCells[(size.y - 1)][i].SetRect(stPos, cellSize);
		}

		//������ � ����� �������
		if (kY != 0)
		for (int i = 0; i < size.y; i++)
		{
			stPos.x = textureRect->left;
			stPos.y = textureRect->top + textureRect->height / ((rand()) % 16 + 1);
			coridorCells[i][0].SetRect(stPos, cellSize);

			stPos.x = textureRect->left + textureRect->width - 16;
			stPos.y = textureRect->top + textureRect->height / ((rand()) % 16 + 1);
			coridorCells[i][size.x - 1].SetRect(stPos, cellSize);
		}		

	}
}






Coridor::~Coridor()
{
}

void Coridor::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	for (int i = 0; i < size.y; i++)
		for (int j = 0; j < size.x; j++)
			target.draw(coridorCells[i][j]);
}
