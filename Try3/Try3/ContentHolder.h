#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

enum SceneType
{
	_MainMenu,
	_Game,
	_Video
};


class ContentHolder
{
public:
	ContentHolder();
	~ContentHolder();



	sf::Texture & getTexture(std::string path);
	sf::Font & getFont(std::string path);
	//sf::SoundBuffer getSound(std::string path);

private:
	std::map<std::string, sf::Texture> textures;
	std::map<std::string, sf::Font> fonts;
	//std::map<std::string, sf::SoundBuffer> sounds;

};

