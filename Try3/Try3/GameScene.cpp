#include "GameScene.h"



GameScene::GameScene(Settings *settings, ContentHolder* _contentHolder, Player* _player)
{
	player = _player;
	contentHolder = _contentHolder;
	s = settings;
	sType = _Game;
}


GameScene::~GameScene()
{
}

void GameScene::Update(sf::Vector2i mPos)
{
}

void GameScene::Update(sf::Event e)
{
	if (e.type == sf::Event::KeyPressed)
	{

	}
	else if (e.type == sf::Event::KeyReleased)
	{

	}
}

void GameScene::Update()
{
	player->Update();
}

void GameScene::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(*player);
	target.draw(player->getInventory());
}
